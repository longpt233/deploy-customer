# deploy-customer
user service for microservice

# gitlab ci 

### ci/cd status

[![pipeline status](https://gitlab.com/longpt233/deploy-customer/badges/main/pipeline.svg)](https://gitlab.com/longpt233/deploy-customer/-/commits/main)

### notes 

- khái niệm gitlab runner tương tự như worker củan jenkin 
- cho phép cài cái runner này ở máy (nếu k thì nó cho free)
- https://www.digitalocean.com/community/tutorials/how-to-set-up-a-continuous-deployment-pipeline-with-gitlab-ci-cd-on-ubuntu-18-04
- các biến môi trường cho file gitlab-ci mà cần bảo mật(pass) cấu hình trên gitlab/setting/cicd/variable

# deploy k8s 

- install test

```aidl
sudo bash install-minikube-test.sh
kubectl apply -f deploy.yaml
```

- intall trên azuze

```aidl
hello
```

- cấu hình cho gitlab -> cluster k8s azuze