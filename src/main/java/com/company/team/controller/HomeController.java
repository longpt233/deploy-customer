package com.company.team.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/")
public class HomeController {

    @GetMapping("/home")
    public ResponseEntity<?> home() {
        return new ResponseEntity<>("Long xin chao ban Mo xinh dep nhat",HttpStatus.OK);
    }
}